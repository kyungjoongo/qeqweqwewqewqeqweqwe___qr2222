// Create a single supabase client for interacting with your database
//const supabase = createClient('https://ekjgqtfivmgvirwrgsmw.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzMzIwOTE0MiwiZXhwIjoxOTQ4Nzg1MTQyfQ.-fhQVEZCPFUMiphvoct8KJaCihppsOgCKEoLjSzvkbw')

import React, {useEffect, useRef, useState} from "react";
import {Alert, AsyncStorage, BackHandler, View,} from "react-native";
import WebView from "react-native-webview";
import * as SplashScreen from 'expo-splash-screen';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';


function sleep(ms) {
    return new Promise(
        resolve => setTimeout(resolve, ms)
    );
}

async function delay_splash() {
    await SplashScreen.preventAutoHideAsync();
    await sleep(1200);
    await SplashScreen.hideAsync();
};

export function callApiSubscribeTopic(topic: string = 'Car') {
    //   return instance.post('/push');
    return messaging().subscribeToTopic(topic).then(() => {
        alert(`${topic} 구독 성공!!`);

        console.info("topic====>", topic);
    }).catch(() => {
        alert(`${topic} 구독 실패! ㅜㅜ`);
    });
}

export function callApiUnsubscribeTopic(topic: string = 'Car') {
    //   return instance.post('/push');
    return messaging().unsubscribeFromTopic(topic).then(() => {
        alert(`${topic} 구독 취소 성공!!`);

        console.info("topic====>", topic);
    }).catch(() => {
        alert(`${topic} 구독 취소 실패! ㅜㅜ`);
    });
}


export default function WebviewScreen(props) {
    const webView = useRef('');

    const [canGoBack, setCanGoBack] = useState(false);
    let _unsubscribe = undefined
    const onAndroidBackPress = (): boolean => {
        if (canGoBack) {
            webView.current.goBack();
            return true; // prevent default behavior (exit app)
        }

        Alert.alert(
            "경고",
            "앱을 종료 하사겠습니까?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK", onPress: () => {
                        BackHandler.exitApp();
                    }
                }
            ]
        );
        return true;

    };
    useEffect((): (() => void) => {
        BackHandler.addEventListener('hardwareBackPress', onAndroidBackPress);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', onAndroidBackPress);
        };
    }, [canGoBack]); //


    useEffect(() => {
        //delay_splash();
        _unsubscribe = props.navigation.addListener('focus', async () => {
            let data = await AsyncStorage.getItem('qr_data')
            if (data !== null) {
                webView.current.postMessage(data)
            }
        });
        return () => {
            _unsubscribe();
        };
    }, [])


    const handlePushMessage = () => {
        alert('sdlfksdlfksdlkf')
    }

    const handleSubscribeTopic = async (pData) => {


        let _data = JSON.parse(pData)

        let qprojectId = _data.projectId;
        let userId = _data.userId;
        let projectName = _data.projectName;
        await callApiSubscribeTopic(qprojectId)
        let token = await AsyncStorage.getItem('fcmToken')
        try {
            let result = await firestore().collection('topic_subscriber').add({
                token: token,
                userId: userId,
                id_token: userId + ";" + token,
                projectName: projectName,
                topic: qprojectId,
                created: new Date(),
            })
            alert('insert 성공!')
        } catch (e) {
            alert(e.toString())
        }
    }


    return (
        <View style={{flex: 1}}>
            <WebView
                onLoadProgress={({nativeEvent}) => {
                    setCanGoBack(nativeEvent.canGoBack)
                }}

                //source={{uri: 'https://install.dev.wisenetcloud.com'}}
                source={{uri: 'http://kyungjoon2.ipdisk.co.kr:3000'}}
                originWhitelist={['*']}
                ref={webView}
                //source={{uri: 'http://kyungjoon.ipdi:3000'}}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                forceDarkOn={true}
                useWebKit={true}
                injectedScripts={`
                    navigator.serviceWorker.getRegistrations().then(function(registrations) {
                       for (let registration of registrations) {
                           registration.unregister();
                       }
                    });
                `}
                //todo : web 으로 부터 전달된 메세지 처리.
                onMessage={(event) => {
                    const data = event.nativeEvent.data
                    if (data === 'OPEN_QR_SCANNER') {
                        props.navigation.push('QrScannerScreen')
                    } else if (JSON.parse(data).key === 'SUB') {
                        handleSubscribeTopic(data)
                    }

                }}
                style={{marginTop: 20, backgroundColor: 'black'}}
            />
        </View>
    );

}
