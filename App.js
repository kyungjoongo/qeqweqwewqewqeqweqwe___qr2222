import * as React from 'react';
import {NavigationContainer,} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import WebviewScreen from "./Screens/WebviewScreen";
import QrScannerScreen from "./Screens/QrScannerScreen";
import {RootSiblingParent} from 'react-native-root-siblings';
import OneSignal from "react-native-onesignal";
//import AnimatedSplash from "react-native-animated-splash-screen";
import * as SplashScreen from 'expo-splash-screen';
import messaging from '@react-native-firebase/messaging'
import './InitOneSignal'
import {Alert, AsyncStorage, Platform} from "react-native";

const TransitionScreenOptions = {
    ...TransitionPresets.SlideFromRightIOS, // This is where the transition happens
};


const Stack = createStackNavigator();


type Props = {};
type State = {
    isLoaded: boolean,
};

export default class App extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false
        }
    }

    async componentDidMount() {
        await SplashScreen.preventAutoHideAsync().then(() => {
            setTimeout(async () => {
                await SplashScreen.hideAsync();
            }, 2500)
        })
        const pushToken = (await OneSignal.getDeviceState()).userId
        console.info("pushToken====>", pushToken);


        await this.handlePushToken();
        await this.messageListener();


    }

    messageListener = async () => {
        console.log('inside message listener ****** ')

        messaging().onMessage(async remoteMessage => {

            console.info("remoteMessage====>", remoteMessage.notification.body);

            Alert.alert(remoteMessage.notification.title, remoteMessage.notification.body);
        });
    }

    handlePushToken = async () => {
        const enabled = await messaging().hasPermission()
        if (enabled) {
            const fcmToken = await messaging().getToken()
            if (fcmToken) {
                console.info("fcmToken====>", fcmToken);
                await AsyncStorage.setItem('fcmToken', fcmToken)
            }
        } else {
            await messaging().requestPermission();
            /*const authorizaed = await messaging.requestPermission
            if (authorized) setAuthorized(true)*/
        }
    }


    render() {
        return (
            <RootSiblingParent>
                <NavigationContainer>
                    <Stack.Navigator screenOptions={TransitionScreenOptions}>
                        <Stack.Screen
                            name="Home" component={WebviewScreen} {...this.props}

                            options={{
                                headerShown: false,
                            }}
                        />
                        <Stack.Screen
                            options={{
                                headerShown: false,
                            }}
                            name="QrScannerScreen"
                            component={QrScannerScreen}
                            {...this.props}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </RootSiblingParent>
        );
    };
};
