const functions = require("firebase-functions");

const express = require("express");
const cors = require("cors");
const admin = require("firebase-admin")
const serviceAccount = require('./firebase-adminsdk.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const userApp = express();
const pushApp = express();

userApp.post('/', async (req, res, next) => {
    const user = req.body;
    await admin.firestore().collection('users').add(user);
    res.status(201).send()
})

userApp.get('/', async (req, res, next) => {

    const snapshot = await admin.firestore().collection('users').get();
    let users = [];
    snapshot.forEach(doc => {

        let id = doc.id;
        let data = doc.data();

        users.push({
            id,
            ...data
        });
    })
    res.status(200).send(JSON.stringify(users))

})

userApp.get("/:id", async (req, res, next) => {
    const snapshot = await admin.firestore().collection('users').doc(req.params.id).get();

    const userId = snapshot.id;
    const userData = snapshot.data;

    res.status(200).send(JSON.stringify({
        id: userId,
        ...userData
    }))
})


userApp.put("/:id", async (req, res) => {

    const body = req.body;

    await admin.firestore().collection('users').doc(req.params.id).update({
        ...body
    })

    res.status(200).send()

})


exports.user = functions.region('asia-northeast3').https.onRequest(userApp)


pushApp.get("/", async (req, res, next) => {
    let fcm_message = {

        notification: {
            title: '프로젝트:asdsdasdasdsdasdasd',
            body: '프로젝트의 변경사항이 생겼습니다'
        },
        data: {
            fileno: '44',
            style: 'goosdfsdkf!!',
        },
        //token: fcm_target_token,
        topic: '0445527c-2f7f-4b5e-80fb-6aba3248284e'
    }
    let result = await admin.messaging().send(fcm_message)

    res.status(200).send({
        result: "ok"
    })
})

pushApp.get("/:topic", async (req, res, next) => {
//.where('topic', '==', req.params.topic)

    const snapshot = await admin.firestore().collection('topic_subscriber').where('topic', '=', req.params.topic).get()

    let rowOne = {}
    snapshot.forEach(function (doc) {
        console.log(doc.id, " => ", doc.data());

        rowOne = {
            id: doc.id,
            ...doc.data(),
        }
    });

    let fcm_message = {

        notification: {
            title: rowOne.projectName,
            body: '프로젝트의 변경사항이 생겼습니다'
        },
        data: {
            fileno: '44',
            style: 'goosdfsdkf!!',
        },
        //token: fcm_target_token,
        topic: req.params.topic
    }
    let result = await admin.messaging().send(fcm_message)
    res.set('Access-Control-Allow-Origin', '*');
    res.status(200).send({
        result: "ok"
    })
})

pushApp.get("/test", async (req, res, next) => {

    const snapshot = await admin.firestore().collection('topic_subscriber').get();
    let results = [];
    snapshot.forEach(doc => {

        let id = doc.id;
        let data = doc.data();

        results.push({
            id,
            ...data
        });
    })
    res.status(200).send(JSON.stringify(results))

})

exports.pushApp = functions.region('asia-northeast3').https.onRequest(pushApp)

