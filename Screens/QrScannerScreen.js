import React, {Component} from 'react';
import {check, PERMISSIONS, request, requestMultiple, RESULTS} from 'react-native-permissions';
import {
    AsyncStorage,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import {FancyAlert} from 'react-native-expo-fancy-alerts';


type Props = {
    navigation: any,
};
type State = {
    camaraGranted: boolean,
    visible: boolean,
};


export default class QrScannerScreen extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            camaraGranted: false,
            visible: false,
        }
    }

    onSuccess = async e => {
        //alert(e.data)
        await this.setState({
            visible: true,
        })
        await AsyncStorage.setItem('qr_data', e.data.toString());
        this.props.navigation.goBack();
    };


    handleCameraPermissionForIos = async () => {
        const res = await check(PERMISSIONS.IOS.CAMERA);
        if (res === RESULTS.GRANTED) {
            this.setState({
                camaraGranted: true,
            })
        } else {
            const res2 = await request(PERMISSIONS.IOS.CAMERA);
            res2 === RESULTS.GRANTED ? this.setState({
                camaraGranted: true,
            }) : this.setState({
                camaraGranted: false,
            });

        }
    }

    async componentWillMount() {
        if (Platform.OS === 'ios') {
            await this.handleCameraPermissionForIos()
        } else {
            await this.handleCamaraPermissionForAndroid()
        }
    }

    handleCamaraPermissionForAndroid() {
        requestMultiple([PERMISSIONS.ANDROID.CAMERA]).then((statuses) => {
            //alert(statuses[PERMISSIONS.ANDROID.CAMERA])
        });
    }

    componentDidMount() {

    }

    render() {


        return (
            <View style={{flex: 1,}}>
                <QRCodeScanner
                    onRead={this.onSuccess}
                    flashMode={RNCamera.Constants.FlashMode.auto}
                    topContent={
                        <Text style={styles.centerText}>
                            장비의 QR코드를 스캔
                        </Text>
                    }
                   /* cameraProps={{
                        useCamera2Api: true
                    }}*/
                    checkAndroid6Permissions={true}
                    showMarker={true}
                    markerStyle={{
                        borderColor: '#ff7f00'
                    }}
                    containerStyle={{backgroundColor: 'black'}}
                    bottomContent={
                        <View style={{marginTop: 20}}>
                            <TouchableOpacity style={styles.buttonTouchable}
                                              onPress={async () => {
                                                  await AsyncStorage.setItem('qr_data', '');
                                                  this.props.navigation.goBack()
                                              }}
                            >
                                <Text style={styles.buttonText}>Close</Text>
                            </TouchableOpacity>
                        </View>
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        marginTop: 70,
        fontSize: 17,
        padding: 32,
        color: 'orange'
    },
    textBold: {
        fontWeight: '500',
        color: '#000'
    },
    buttonText: {
        fontSize: 15,
        color: 'white'
    },
    buttonTouchable: {
        padding: 16,
        height: 50,
        borderRadius:5,
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        backgroundColor: '#ff7f00'
    }
});

