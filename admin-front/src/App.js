import './App.css';
import {Button, message} from "antd";

import React, {useEffect, useState} from 'react';
import firebase from "firebase";
import 'firebase/firestore';
import {Text, View} from "react-native-web";
import {Table} from "react-bootstrap";
import axios from "axios";
import {ActivityIndicator, TouchableOpacity} from "react-native";

const firebaseConfig = {
    apiKey: "AIzaSyBBYUvtf-e2BNYABXwF3AN8uQljAAu-R-M",
    authDomain: "camportalsplash.firebaseapp.com",
    projectId: "camportalsplash",
    storageBucket: "camportalsplash.appspot.com",
    messagingSenderId: "42102214308",
    appId: "1:42102214308:web:f83521becc77d48c8a2031",
    measurementId: "G-H0P36GNL4V"
};

firebase.initializeApp(firebaseConfig) //firebase 초기화

const db = firebase.firestore(); //store 사용

function App() {

    const [results, setResults] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getList();
    }, [])

    async function getList() {

        setLoading(true)
        const snapshot = await db.collection('topic_subscriber').get();
        let _results = [];
        snapshot.forEach(doc => {
            let id = doc.id;
            let data = doc.data();

            _results.push({
                id,
                ...data
            });
        })
        _results.sort((a, b) => b.created - a.created);
        console.info("temp====>", _results);
        setResults(_results)
        setLoading(false)
    }

    /*id: "pWjmQ73DcVrTQ8DBIJxA"
    id_token: "dolchulab@gmail.com;f8XiTaqnS8qjTTF84n12xM:APA91bHnly-30rv57PSKcP2GXlWSZBpFktwnQfY1MZQTEa80lYiyRQl7xanKFO9ryr7Jrqd7cRnmoczYvGIhourHW9SGOmprpRNNr1en-3ORzqfsM3uM7utwlCzoRddDIUpXXlUFMEYo"
    projectName: "dsgsdfgsdfg"
    token: "f8XiTaqnS8qjTTF84n12xM:APA91bHnly-30rv57PSKcP2GXlWSZBpFktwnQfY1MZQTEa80lYiyRQl7xanKFO9ryr7Jrqd7cRnmoczYvGIhourHW9SGOmprpRNNr1en-3ORzqfsM3uM7utwlCzoRddDIUpXXlUFMEYo"
    topic: "c825a6b9-8070-4b63-b97c-df19313cdffe"
    userId: "dolchulab@gmail.com"
        [[Prototype]]: Object*/

    return (
        <div style={{margin: 10,}}>
            <View style={{margin: 5,}}>
                <Button disabled={true} s
                        style={{backgroundColor: 'orange', color: 'black', fontWeight: 'bold'}}
                        type="primary">install 프로젝트 구독자 리스트.</Button>
            </View>
            {loading && <View style={{position: 'absolute', top: 100, left: '48%'}}>
                <ActivityIndicator color={'orange'} size={'large'}/>
            </View>}
            <div>
                <Table striped bordered hover size="sm" responsive>
                    <thead>
                    <tr>
                        <th size={12}>##</th>
                        <th>topic</th>
                        <th>projectName</th>
                        <th>userId</th>
                        <th>device_token</th>
                        <th>id</th>
                        <th>id_token</th>

                    </tr>
                    </thead>
                    <tbody>
                    {results.map(item => {
                        return (
                            <tr>
                                <th>
                                    <Button
                                        style={{height: 50, marginTop: 10,}}
                                        onClick={async () => {
                                            try {
                                                setLoading(true)
                                                let res = await axios({
                                                    url: `https://asia-northeast3-camportalsplash.cloudfunctions.net/pushApp/${item.topic}`,
                                                    method: 'get',
                                                })
                                                setLoading(false)
                                                if (res.data.result === 'ok') {
                                                    message.info('push noti sucess!!')
                                                } else {
                                                    alert('문제 있다')
                                                }
                                            } catch (e) {
                                                alert(e.toString())
                                            }
                                        }}
                                    >
                                        <View style={{flexDirection: 'row', width: 250,}}>
                                            <Text style={{
                                                color: 'blue',
                                                fontWeight: 'bold',
                                                fontSize: 12
                                            }}>{item.projectName}</Text>
                                            <Text style={{fontSize: 12}}>구독자에게 push-noti</Text>
                                        </View>
                                    </Button>
                                </th>
                                <th>{item.topic}</th>
                                <th>{item.projectName}</th>
                                <th>{item.userId}</th>
                                <th>{item.token}</th>

                                <th>{item.id}</th>
                                <th>{item.id_token}</th>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>

            </div>


        </div>
    );
}

export default App;
